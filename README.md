Java Code Styles
================

IntelliJ IDEA code style settings for Payu's (forked from square) Android projects.


Installation
------------

 * Run the `install.sh` script.
 * Restart IntelliJ if it's running.
 * Open IntelliJ Project Settings -> Code Styles, change the code style for the
   project to the one you want.
